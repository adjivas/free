FROM rust:1.76.0 as build

WORKDIR /usr/src/free
COPY . .
RUN cargo install --path .

FROM debian
RUN apt-get update \
    && apt-get install -y build-essential \
                          python3 python3-pandas python3-seccomp \
    && rm -rf /var/lib/apt/lists/*
COPY --from=build /usr/local/cargo/bin/free /usr/local/bin/free
ENV HOST_ADDR="0.0.0.0:3000"
ENV PYTHON="python3"
CMD ["free"]
