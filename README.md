# Free

[![pipeline status](https://gitlab.com/adjivas/free/badges/master/pipeline.svg)](https://gitlab.com/adjivas/free/-/commits/master)

This project is a WEB service, the users can send Python code and receive ID.
With this ID, the users can run their code into sandboxed python,
And receive the execution result.

The default installed Python packages are numpy and pandas.

## The sandbox
The service sandbox strategy is: Resource limits+Secure Computing 
The studied options were:
| RestrictedExecution | RustPython           | Resource limits+Secure Computing |
| :-----------------: | :------------------: | :------------------------------: |
| + OS-agnostic       | + VM WASM Sandboxing | + Unix Sandboxing                |
| -- Not a sandbox    | -- pandas support    | - Complexity                     |
| -- proba futurs CVE |                      |                                  |

## Play with it!
How to configure the environment:
```shell
> cp env.example.sh env.sh
> $EDITOR env.sh
> source env.sh
```

How to run this service:
```shell
> cargo run --release
...
listening on 127.0.0.1:3000
```

How to use it:
```shell
> curl -X POST --data-binary "print('hello')" 127.0.0.1:3000/send
00000000-0000-0000-0000-000000000043
> curl -X GET http://127.0.0.1:3000/run/00000000-0000-0000-0000-000000000043
{"code":0,"error":"","output":"hello\n"}
```

## Code with it!
### Livecoding/Testing this project
How to run the tests and livecoding:
```shell
> cargo watch -- cargo test
...
```

### Documentation
How to build the doc:
```shell
> cargo doc
...
```

### Containerization using Docker
```shell
> docker-compose up --build
[+] Running 1/0
 ✔ Container free-free-1  Created                                                                                          0.0s 
Attaching to free-1
free-1  | listening on 0.0.0.0:3000
```

### Acknowledge
Some helpfull ressources:
* [2020-10-27, Using seccomp - Making your applications more secure](https://spinscale.de/posts/2020-10-27-seccomp-making-applications-more-secure.html)
* [2023-08-31, Timeout With Command in Tokio](https://blog.juliobiason.me/code/tokio-command-timeout-test)
