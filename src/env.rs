//! This module contains the default environment variables
//! and the environment structure.

use std::env::var;

/// This is a limit, in seconds,
/// on the amount of CPU time that the process will consume.
const DEFAULT_RLIMIT_CPU: &'static str = "10";
/// This is the maximum size, in bytes, of the process's virtual memory.
// 100*1024^2 Bytes = 100 Mo of virtual memory
const DEFAULT_RLIMIT_AS: &'static str = "104857600";
// 10*1024^2 Bytes = 10 MB's python code
/// This is the maximum size, in bytes, of the allowed source code.
const DEFAULT_BODY_LIMIT: &'static str = "10485760";
/// This is the IP address and port of the server.
const DEFAULT_HOST_ADDR: &'static str = "127.0.0.1:3000";
/// This is the python binary name.
const DEFAULT_PYTHON: &'static str = "python3";

/// The `Environment` structure used by the state.
#[derive(Clone, Copy)]
pub struct Environment {
    /// The limit, in seconds, on the amount of CPU time that the process will consume.
    pub rlimit_cpu: usize,
    /// The maximum size, in bytes, of the process's virtual memory.
    pub rlimit_as: usize,
    /// The maximum size, in bytes, of the allowed source code.
    pub body_limit: usize,
    /// The IP address and port of the server.
    pub host_addr: &'static str,
    /// The python binary name.
    pub python: &'static str,
}

impl Default for Environment  {
    /// Returns an `Environment` structure built from the `os` Environment.
    fn default() -> Self {
        Environment {
            rlimit_cpu: 
                var("RLIMIT_CPU").unwrap_or(DEFAULT_RLIMIT_CPU.to_string())
                                 .parse::<usize>().unwrap(),
            rlimit_as: 
                var("RLIMIT_AS").unwrap_or(DEFAULT_RLIMIT_AS.to_string())
                                 .parse::<usize>().unwrap(),
            body_limit:
                var("BODY_LIMIT").unwrap_or(DEFAULT_BODY_LIMIT.to_string())
                                 .parse::<usize>().unwrap(),
            host_addr: Box::leak(
                var("HOST_ADDR").unwrap_or(DEFAULT_HOST_ADDR.to_string())
                                .into_boxed_str()
            ),
            python: Box::leak(
                var("PYTHON").unwrap_or(DEFAULT_PYTHON.to_string())
                             .into_boxed_str()
            ),
        }
    }
}
