//! This module contains the safe python executor.

#[cfg(test)]
mod test;
mod safecode;

use super::env::{self, Environment};

use safecode::Safecode;
use tokio::process::Command;

use std::{
  error::Error,
  process::{Output, Stdio},
};

/// The function returns the full command to run the python user's code,
/// This command puts the user's code into a sandbox.
/// The sandbox is configured by the environment.
fn get_command(
    code: &str,
    env: &Environment,
) -> Command {
    let safecode = Safecode(env).to_string();
    let code = format!(r#"{safecode}
try:
  exec("""{code}""");
except Exception as error:
  import os; print(error, file=sys.stderr); os._exit(1);"#);

    let mut command = Command::new("/usr/bin/env");

    command.arg(&env.python);
    command.arg(&"-c");
    command.arg(&code);
    command.stdout(Stdio::piped());
    command.stderr(Stdio::piped());
    command
}

/// The function runs the source code in a safe context
/// and returns the result of the execution.
pub async fn run_with_timelapse(
    code: &str,
    env: &Environment,
) -> Result<Output, Box<dyn Error>> {
    let mut command = get_command(code, env);
    let child = command.spawn()?;
    let output = child.wait_with_output().await?;

    Ok(output)
}
