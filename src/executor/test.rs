use super::run_with_timelapse;

#[tokio::test]
async fn should_run_simple_code() {
    let code = "print(43);";
    let env = Default::default();
    let output = run_with_timelapse(code, &env).await.unwrap();

    assert_eq!(Ok("43\n"), std::str::from_utf8(&output.stdout));
    assert_eq!(Ok(""), std::str::from_utf8(&output.stderr));
    assert_eq!(0, output.status.code().unwrap_or_default());
}

#[tokio::test]
async fn should_run_simple_pandas_code() {
    let code = "import pandas;";
    let env = Default::default();
    let output = run_with_timelapse(code, &env).await.unwrap();

    assert_eq!(Ok(""), std::str::from_utf8(&output.stdout));
    assert_eq!(Ok(""), std::str::from_utf8(&output.stderr));
    assert_eq!(0, output.status.code().unwrap_or_default());
}

#[tokio::test]
async fn should_run_with_timelapse() {
    let code = "print(43);";
    let env = Default::default();
    let output = run_with_timelapse(code, &env).await.unwrap();

    assert_eq!(Ok("43\n"), std::str::from_utf8(&output.stdout));
    assert_eq!(Ok(""), std::str::from_utf8(&output.stderr));
    assert_eq!(0, output.status.code().unwrap_or_default());
}

#[tokio::test]
async fn should_run_with_outdaded_timelapse() {
    let code = concat!(
        "while True:",
        "  continue ;"
    );
    let env = Default::default();
    let output = run_with_timelapse(code, &env).await.unwrap();

    assert_eq!(Ok(""), std::str::from_utf8(&output.stdout));
    assert_eq!(Ok(""), std::str::from_utf8(&output.stderr));
    assert_eq!(0, output.status.code().unwrap_or_default());
}

#[tokio::test]
async fn should_run_invalid_code() {
    let code = "print(NOT_EXIST);";
    let env = Default::default();
    let output = run_with_timelapse(code, &env).await.unwrap();

    let expected_error = "name 'NOT_EXIST' is not defined\n";
    assert_eq!(Ok(""), std::str::from_utf8(&output.stdout));
    assert_eq!(Ok(expected_error), std::str::from_utf8(&output.stderr));
    assert_eq!(1, output.status.code().unwrap_or_default());
}

#[tokio::test]
async fn should_run_with_allocation() {
    let code = concat!(
        r#"import numpy;"#,
        r#"from sys import getsizeof;"#,
        r#"allocated = numpy.empty(1024, dtype=numpy.int8);"#,
        r#"print(getsizeof(allocated));"#,
        r#"allocated.fill(0);"#,
    );
    let env = Default::default();
    let output = run_with_timelapse(code, &env).await.unwrap();

    assert_eq!(Ok("1136\n"), std::str::from_utf8(&output.stdout));
    assert_eq!(Ok(""), std::str::from_utf8(&output.stderr));
    assert_eq!(0, output.status.code().unwrap_or_default());
}

#[tokio::test]
async fn should_run_with_too_big_allocation() {
    let code = concat!(
        r#"import numpy;"#,
        r#"from sys import getsizeof;"#,
        r#"allocated = numpy.empty(1073741824, dtype=numpy.int8);"#,
        r#"print(getsizeof(allocated));"#,
        r#"allocated.fill(0);"#,
    );
    let env = Default::default();
    let output = run_with_timelapse(code, &env).await.unwrap();

    assert_eq!(Ok(""), std::str::from_utf8(&output.stdout));
    assert_eq!(Ok(""), std::str::from_utf8(&output.stderr));
    assert_eq!(0, output.status.code().unwrap_or_default());
}
