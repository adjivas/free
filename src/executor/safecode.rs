//! This module contains the safe code to run.

use super::env::Environment;

use std::fmt;

pub struct Safecode<'a>(pub &'a Environment);

impl <'a>fmt::Display for Safecode<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let Safecode(env) = self;
        let rlimit_cpu = env.rlimit_cpu;
        let rlimit_as = env.rlimit_as;

        write!(f, concat!(
          r#"import seccomp;"#,
          r#"import sys;"#,
          r#"import pandas;"#,
          r#"import resource;"#,
          r#"resource.setrlimit(resource.RLIMIT_CPU, ({}, {}));"#,
          r#"resource.setrlimit(resource.RLIMIT_AS, ({}, {}));"#,
          r#"filter = seccomp.SyscallFilter(seccomp.ERRNO(seccomp.errno.EPERM));"#,
          r#"filter.add_rule(seccomp.ALLOW, "munmap");"#,
          r#"filter.add_rule(seccomp.ALLOW, "mmap");"#,
          r#"filter.add_rule(seccomp.ALLOW, "write", seccomp.Arg(0, seccomp.EQ, sys.stdout.fileno()));"#,
          r#"filter.add_rule(seccomp.ALLOW, "write", seccomp.Arg(0, seccomp.EQ, sys.stderr.fileno()));"#,
          r#"filter.add_rule(seccomp.ALLOW, "exit_group");"#,
          r#"filter.load();"#), rlimit_cpu, rlimit_cpu, rlimit_as, rlimit_as)
    }
}
