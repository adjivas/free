//! This project is a WEB service,
//! The users can send Python code and receive ID.
//! ```shell
//! > curl -X POST --data-binary "@file43.py" 127.0.0.1:3000/send
//! {"uuid":" MY_ID}
//! > curl -X POST --data-binary "print(43)" 127.0.0.1:3000/send
//! {"uuid":" MY_ID}
//! ```
//! With this ID, the users can run their code into sandboxed python,
//! And receive the execution result.
//! ```shell
//! > curl -X GET http://127.0.0.1:3000/run/MY_ID | jq
//! {
//!   "code": 0,
//!   "error": ""
//!   "output": "43\n",
//! }
//! ```

mod executor;
mod router;
mod env;

use std::error::Error;
use std::sync::Arc;

use env::Environment;
use dashmap::DashMap;
use uuid::Uuid;

/// The AppState structure contains the database and environment fields.
#[derive(Default)]
struct AppState {
    /// The Database field
    db: DashMap<Uuid, String>,
    /// The Environment field
    env: Environment
}

/// The `SharedState` shares `AppState` between the paths of this HTTP server.
type SharedState = Arc<AppState>;

/// The main function runs the HTTP server
/// with an IP address and port defined in the environment.
#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let env = Environment::default();
    let state = Default::default();
    let app = router::get_routes(state);
    let listener = tokio::net::TcpListener::bind(env.host_addr).await?;

    println!("listening on {}", listener.local_addr()?);
    axum::serve(listener, app).await?;
    Ok(())
}
