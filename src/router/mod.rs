//! This module contains the router and the paths.

#[cfg(test)]
mod test;

use axum::{
    routing::{get, post},
    extract::{Path, State, DefaultBodyLimit},
    response::{Json, IntoResponse},
    http::StatusCode,
    Router
};
use tower_http::limit::RequestBodyLimitLayer;
use uuid::Uuid;
use serde_json::json;

use super::executor::run_with_timelapse;
use super::SharedState;

/// This function returns the router of this server
/// with an Environment State that limits the size of the body
pub fn get_routes(state: SharedState) -> Router {
    Router::new()
        .route("/run/:uuid", get(run))
        .route("/send", post(send))
        // [disable](https://docs.rs/axum/latest/axum/extract/struct.DefaultBodyLimit.html#method.disable)
        // Note that if you’re accepting data from untrusted remotes it is recommend to add your own limit such as [tower_http::limit].
        .layer(DefaultBodyLimit::disable())
        .layer(RequestBodyLimitLayer::new(state.env.body_limit))
        .with_state(state)
}

/// This POST function adds the provided source code to the `db`
/// and returns a `CREATED` response with his code's `UUID`.
async fn send(
    State(state): State<SharedState>,
    code: String,
) -> impl IntoResponse {
    let uuid = Uuid::new_v4();

    state.db.insert(uuid, code);
    (StatusCode::CREATED, Json(json!({ "uuid": uuid })))
}

/// This GET function runs a code stored on the `db` with an UUID subpath
/// and returns an `OK` response with the output/error/status result.
async fn run(
    State(state): State<SharedState>,
    uuid: Path<String>,
) -> impl IntoResponse {
    if let Ok(ref uuid) = Uuid::parse_str(&uuid) {
        if let Some(code) = state.db.get(uuid) {
            let result = run_with_timelapse(&code, &state.env).await.unwrap();

            (StatusCode::OK, Json(json!({
                "output": std::str::from_utf8(&result.stdout).unwrap(),
                "error": std::str::from_utf8(&result.stderr).unwrap(),
                "code": result.status.code().unwrap_or_default(),
            })))
        } else {
            (StatusCode::NOT_FOUND, Json(json!({
                "error": "ID not found",
            })))
        }
    } else {
        (StatusCode::BAD_REQUEST, Json(json!({
            "error": "Invalid ID",
        })))
    }
}
