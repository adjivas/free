use super::get_routes;
use super::SharedState;

use axum::http::StatusCode;
use axum_test::TestServer;
use serde::{Deserialize, Serialize};

#[tokio::test]
async fn should_request_send_code() {
    let state = SharedState::default();
    let app = get_routes(state.clone());
    let server = TestServer::new(app).unwrap();

    let expected_code = r#"print("hello world")"#;
    let response = server.post("/send").text(expected_code).await;
    response.assert_status(StatusCode::CREATED);

    let mut it = state.db.iter();
    let code = it.next().unwrap().value().clone();

    assert_eq!(expected_code, code);
}

#[tokio::test]
async fn should_request_send_code_with_to_big_body() {
    let state = SharedState::default();
    let app = get_routes(state.clone());
    let server = TestServer::new(app).unwrap();

    
    let ref source_code = "\0".repeat(state.env.body_limit + 1);
    let response = server.post("/send").text(source_code).await;
    response.assert_status(StatusCode::PAYLOAD_TOO_LARGE);

    let mut it = state.db.iter();
    assert!(it.next().is_none());
}

#[cfg(test)]
#[derive(Serialize, Deserialize, Debug)]
struct CodePlayId {
    uuid: String
}

#[tokio::test]
async fn should_request_run_code() {
    let state = SharedState::default();
    let app = get_routes(state.clone());
    let server = TestServer::new(app).unwrap();

    let source_code = r#"print("hello world")"#;
    let response = server.post("/send").text(source_code).await;
    let CodePlayId { uuid } = response.json::<CodePlayId>();

    let response = server.get(&format!("/run/{uuid}")).text(source_code).await;

    response.assert_status_ok();
    response.assert_text(r#"{"code":0,"error":"","output":"hello world\n"}"#);
}

#[tokio::test]
async fn should_request_run_code_with_not_found_uuid() {
    let state = SharedState::default();
    let app = get_routes(state.clone());
    let server = TestServer::new(app).unwrap();

    let uuid = "00000000-0000-0000-0000-000000000000";
    let source_code = r#"print("hello world")"#;
    let response = server.get(&format!("/run/{uuid}")).text(source_code).await;

    response.assert_status_not_found();
    response.assert_text(r#"{"error":"ID not found"}"#);
}

#[tokio::test]
async fn should_request_run_code_with_invalid_uuid() {
    let state = SharedState::default();
    let app = get_routes(state.clone());
    let server = TestServer::new(app).unwrap();

    let uuid = "INVALID";
    let source_code = r#"print("hello world")"#;
    let response = server.get(&format!("/run/{uuid}")).text(source_code).await;

    response.assert_status_bad_request();
    response.assert_text(r#"{"error":"Invalid ID"}"#);
}
